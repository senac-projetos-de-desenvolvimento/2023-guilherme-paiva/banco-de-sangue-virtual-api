import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';

export const ReceberDoacao = sequelize.define('ReceberDoacao', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    email: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    fone: {
        type: DataTypes.STRING(12),
        allowNull: false
    },
    tiposanguineo: {
        type: DataTypes.STRING(8),
        allowNull: false
    },
    localdoacao: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
}, {
    tableName: "Receber Doação"
});
