import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';

export const Apoiador = sequelize.define('Apoiador', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    email: {
        type: DataTypes.STRING(80),
        allowNull: false
    },
    fone: {
        type: DataTypes.STRING(12),
        allowNull: false
    },
    mensagem: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
}, {
    tableName: "Apoiadores"
});
