import { DataTypes } from "sequelize";
import { sequelize } from "../databases/conecta.js";

export const Doador = sequelize.define(
  "Doador",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nome: {
      type: DataTypes.STRING(80),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(80),
      allowNull: false,
    },
    fone: {
      type: DataTypes.STRING(12),
      allowNull: false,
    },
    estado: {
      type: DataTypes.STRING(40),
      allowNull: false,
    },
    cidade: {
      type: DataTypes.STRING(40),
      allowNull: false,
    },
    sexo: {
      type: DataTypes.STRING(8),
      allowNull: false,
    },
    tiposanguineo: {
      type: DataTypes.STRING(8),
      allowNull: false,
    },
    ondenosconheceu: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
  },
  {
    tableName: "Doadores",
  }
);
