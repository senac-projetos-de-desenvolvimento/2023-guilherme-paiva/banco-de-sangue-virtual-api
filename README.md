# Banco de Sangue Virtual Api

O Banco de Sangue Virtual App é um projeto desenvolvido por Guilherme Paiva e André Geraldo na disciplina de Projeto de Desenvolvimento I e II do curso de analise e desenvolvimento de sistemas da faculdade Senac/RS. Este aplicativo tem como objetivo facilitar o processo de doação de sangue, conectando doadores e receptores de sangue de forma eficiente e segura.

## Funcionalidades

O Banco de Sangue Virtual App oferece as seguintes funcionalidades:

- Cadastro de doadores e receptores de sangue
- Busca de doadores por tipo sanguíneo e região
- Notificações e lembretes para doadores
- Recursos de segurança para garantir a privacidade dos usuários

## Tecnologias utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias:

- JavaScript: Linguagem de programação utilizada tanto no frontend quanto no backend do aplicativo.
- React Native: Um framework JavaScript para o desenvolvimento de aplicativos móveis multiplataforma.
- Expo: Uma plataforma e conjunto de ferramentas para o desenvolvimento de aplicativos React Native.
- Node.js: Um ambiente de execução JavaScript do lado do servidor.
- Express: Um framework web rápido e minimalista para Node.js.
- Sequelize: Um ORM (Object-Relational Mapping) para Node.js, utilizado para interagir com o banco de dados.

## Instalação

Para executar o Banco de Sangue Virtual App em seu dispositivo, siga as etapas abaixo:

1. Clone o repositório do projeto:

```
git clone https://gitlab.com/senac-projetos-de-desenvolvimento/2023-guilherme-paiva/banco-de-sangue-virtual-api.git
```

2. Certifique-se de ter o Node.js e o npm (gerenciador de pacotes do Node.js) instalados em seu ambiente de desenvolvimento. Para obter mais informações sobre como instalar o Node.js, consulte a documentação oficial: https://nodejs.org

3. Acesse o diretório do projeto:

```
cd banco-de-sangue-virtual-api
```

4. Instale as dependências do backend do projeto:

```
npm install
```

5. Inicie o servidor backend:

```
nodemon app.js
```
