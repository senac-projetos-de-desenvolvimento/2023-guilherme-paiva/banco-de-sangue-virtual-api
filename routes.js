import { Router } from "express";
import { doadorIndex, doadorCreate, dadosEstatisticos, dadosEstatisticosCount, doadorDestroy } from "./controllers/doadorController.js";
import { doacaoIndex, doacaoCreate, criarDoacaoEEnviarEmails, receberDoacaoDestroy } from "./controllers/receberDoacaoController.js";
import { contatoCreate, contatoIndex } from "./controllers/contatoController.js";
import { apoiadorCreate, apoiadorIndex } from "./controllers/apoiadorController.js";
import { emailIndex, enviarEmails } from "./controllers/emailController.js";

const router = Router();

router.get("/doadores", doadorIndex)
    .post("/doadores", doadorCreate)
    .delete("/doadores/:id", doadorDestroy)
    .get("/doadoresDados", dadosEstatisticos)
    .get('/dadosDoadores', dadosEstatisticosCount)

router.get("/doacoes", doacaoIndex)
    .post("/doacoes", doacaoCreate)
    .delete('/doacoes/:id', receberDoacaoDestroy)
    .post("/doacoesEmail", criarDoacaoEEnviarEmails)

router.get("/contatos", contatoIndex)
    .post("/contatos", contatoCreate)

router.get("/apoiadores", apoiadorIndex)
    .post("/apoiadores", apoiadorCreate)

router.get("/email/:cidade/:tipoSanguineo", emailIndex)
      .post("/emails/:cidade", enviarEmails )
export default router;
