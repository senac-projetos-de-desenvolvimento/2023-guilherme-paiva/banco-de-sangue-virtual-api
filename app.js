import express from "express";
import { sequelize } from "./databases/conecta.js";
import routes from "./routes.js";
import cors from "cors";

const app = express();
const port = 3001;

app.use(express.json());
app.use(cors());
app.use(routes);

async function conecta_db() {
  try {
    await sequelize.authenticate();
    console.log("Conexão ao banco de dados realizada com sucesso.");
    await await sequelize.sync();
  } catch (error) {
    console.error("Erro na conexão com o banco:", error);
  }
}
conecta_db();

app.get("/", (req, res) => {
  res.send("Banco de Sangue - API");
});

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});
