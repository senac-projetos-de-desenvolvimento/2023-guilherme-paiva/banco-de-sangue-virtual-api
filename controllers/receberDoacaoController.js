import { Doador } from "../data/Doador.js";
import { ReceberDoacao } from "../data/ReceberDoacao.js";
import { Op } from "sequelize";
import nodemailer from "nodemailer";

export const doacaoIndex = async (req, res) => {
  try {
    // obtém todos os registros da tabela (a partir da Model) Doador
    const doacoes = await ReceberDoacao.findAll();
    res.status(200).json(doacoes);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const doacaoCreate = async (req, res) => {
  const { nome, email, fone, tiposanguineo, localdoacao } = req.body;

  // se não informou estes atributos
  if (!nome || !email || !fone || !tiposanguineo || !localdoacao) {
    res.status(400).json({
      id: 0,
      msg: "Erro... Informe nome, email, fone, tiposanguineio e local para doação.",
    });
    return;
  }

  try {
    const doacoes = await ReceberDoacao.create({
      nome,
      email,
      fone,
      tiposanguineo,
      localdoacao,
    });
    res.status(201).json(doacoes);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const receberDoacaoDestroy = async (req, res) => {
  const { id } = req.params
 
  try {
    await ReceberDoacao.destroy({ where: { id } });

    res.status(200).json({ msg: "Ok! Removido com Sucesso" })
  } catch (error) {
    res.status(400).send(error)
  }
}
const transporter = nodemailer.createTransport({
  // Configurações do serviço de e-mail
  service: "hotmail",
  auth: {
    user: "tccv1senac@outlook.com", // Insira seu e-mail
    pass: "senac2023#", // Insira sua senha do e-mail
  },
});

const doadoresCompativeis = (tipoReceptor) => {

  switch (tipoReceptor) {
    case "O-":
      return ["O-"];
    case "O+":
      return ["O-", "O+"];
    case "A-":
      return ["O-", "A-"];
    case "A+":
      return ["O-", "O+", "A-", "A+"];
    case "B-":
      return ["O-", "B-"];
    case "B+":
      return ["O-", "O+", "B-", "B+"];
    case "AB-":
      return ["O-", "A-", "B-", "AB-"];
    case "AB+":
      return ["O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+"];
    default:
      return [];
  }
};
export const criarDoacaoEEnviarEmails = async (req, res) => {
  const { nome, email, fone, tiposanguineo, localdoacao } = req.body;

  // Verifique se todos os campos necessários estão presentes
  if (!nome || !email || !fone || !tiposanguineo || !localdoacao) {
    res.status(400).json({
      id: 0,
      msg: "Erro... Informe nome, email, fone, tiposanguineio, local para doação e cidade.",
    });
    return;
  }

  try {
    const doacao = await ReceberDoacao.create({
      nome,
      email,
      fone,
      tiposanguineo,
      localdoacao,
    });

    const doadores = await Doador.findAll({
      attributes: ["email", "nome"],
      where: {
        cidade: {
          [Op.eq]: localdoacao,
        },
        tipoSanguineo: {
          [Op.in]: doadoresCompativeis(tiposanguineo),
        },
      },
    });

    for (const doador of doadores) {
      const mailOptions = {
        from: "tccv1senac@outlook.com",
        to: doador.email,
        subject: `Atenção ${localdoacao}! Precisamos de Doação de Sangue na sua cidade`,
        html: `<h3>Boa noite, ${doador.nome}!</h3><h4>Precisamos de doação de sangue do tipo ${tiposanguineo} na cidade de ${localdoacao} </h4>`,
      };

      await transporter.sendMail(mailOptions);
      console.log(`E-mail enviado para: ${doador.email}`);
    }

    res.status(201).json(doacao);
  } catch (error) {
    res.status(400).send(error);
  }
};
