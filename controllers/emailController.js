import { Doador } from "../data/Doador.js";
import { Op } from "sequelize";
import nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
  // Configurações do serviço de e-mail
  service: "hotmail",
  auth: {
    user: "tccv1senac@outlook.com", // Insira seu e-mail
    pass: "senac2023#", // Insira sua senha do e-mail
  },
});

const doadoresCompativeis = (tipoReceptor) => {

  switch (tipoReceptor) {
    case "O-":
      return ["O-"];
    case "O+":
      return ["O-", "O+"];
    case "A-":
      return ["O-", "A-"];
    case "A+":
      return ["O-", "O+", "A-", "A+"];
    case "B-":
      return ["O-", "B-"];
    case "B+":
      return ["O-", "O+", "B-", "B+"];
    case "AB-":
      return ["O-", "A-", "B-", "AB-"];
    case "AB+":
      return ["O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+"];
    default:
      return [];
  }
};

export const emailIndex = async (req, res) => {
  const { cidade, tipoSanguineo } = req.params;
  try {
    const doadores = await Doador.findAll({
      attributes: ["email", "nome"],
      where: {
        cidade: {
          [Op.eq]: cidade,
        },
        tipoSanguineo: {
          [Op.in]: doadoresCompativeis(tipoSanguineo),
        },
      },
    });
    for (const doador of doadores) {
      const mailOptions = {
        from: "tccv1senac@outlook.com",
        to: doador.email,
        subject: `Atenção ${cidade}! Precisamos de Doação de Sangue na sua cidade`,
        html: `<h3>Boa noite, ${doador.nome}!</h3><h4>Precisamos de doação de sangue do tipo ${tipoSanguineo} na cidade de ${cidade} </h4>`,
      };
      await transporter.sendMail(mailOptions);
      console.log(`E-mail enviado para: ${doador.email}`);
    }

    res.status(200).json("Sucess");
  } catch (error) {
    res.status(400).send(error);
  }
};
export const enviarEmails = async (req, res) => {
  const { cidade } = req.params;
  const { assunto, corpo } = req.body;

  try {
    const doadores = await Doador.findAll({
      attributes: ['email', 'nome'],
      where: {
        cidade: {
          [Op.eq]: cidade,
        },
      },
    });

    for (const doador of doadores) {
      const mailOptions = {
        from: "tccv1senac@outlook.com",
        to: doador.email,
        subject: assunto,
        html: `<h3>Olá, ${doador.nome}!</h3><p>${corpo}</p>`,
      };

      await transporter.sendMail(mailOptions);
      console.log(`E-mail enviado para: ${doador.email}`);
    }

    res.status(200).json('Sucesso');
  } catch (error) {
    console.error(error);
    res.status(500).send('Erro ao enviar e-mails');
  }
};