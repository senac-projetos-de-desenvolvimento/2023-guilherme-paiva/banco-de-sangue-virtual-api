import { Apoiador } from "../data/Apoiador.js";
import nodemailer from "nodemailer";

const transporter = nodemailer.createTransport({
  // Configurações do serviço de e-mail
  service: "hotmail",
  auth: {
    user: "tccv1senac@outlook.com", // Insira seu e-mail
    pass: "senac2023#", // Insira sua senha do e-mail
  },
});

export const apoiadorIndex = async (req, res) => {
  try {
    const apoiadores = await Apoiador.findAll();
    res.status(200).json(apoiadores);
  } catch (error) {
    res.status(400).send(error);
  }
};

export const apoiadorCreate = async (req, res) => {
  const { nome, email, fone, mensagem } = req.body;

  if (!nome || !email || !fone || !mensagem) {
    res
      .status(400)
      .json({ id: 0, msg: "Erro... Informe nome, email, fone e mensagem." });
    return;
  }

  try {
    const apoiadores = await Apoiador.create({
      nome,
      email,
      fone,
      mensagem,
    });
    const mailOptions = {
      from: "tccv1senac@outlook.com",
      to: "paiva_lol@hotmail.com",
      subject: `E-mail para se tornar um apoiador ${nome} - ${email}`,
      html: `<h3>Olá! Segue a baixo informações do apoiador </h3><h4>E-mail para se tornar um apoiador ${nome} - ${email}</h4><div><p>Nome: ${nome}</p></div> <div><p>Email: ${email}</p></div> <div><p>Fone: ${fone}</p></div> <div><p>Mensagem: ${mensagem}</p></div>`,
    };
    await transporter.sendMail(mailOptions);
    console.log(`E-mail enviado`);
    res.status(201).json(apoiadores);
  } catch (error) {
    res.status(400).send(error);
  }
};
