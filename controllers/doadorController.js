import { Contato } from "../data/Contato.js";
import { Doador } from "../data/Doador.js";
import { ReceberDoacao } from "../data/ReceberDoacao.js";
import { sequelize } from "../databases/conecta.js";

export const doadorIndex = async (req, res) => {
    try {
    
        const doadores = await Doador.findAll()
        res.status(200).json(doadores)
    } catch (error) {
        res.status(400).send(error)
    }
}

export const doadorCreate = async (req, res) => {
    const { nome, email, fone, estado, cidade, sexo, tiposanguineo, ondenosconheceu } = req.body

    if (!nome || !email || !fone || !estado || !cidade || !sexo || !tiposanguineo || !ondenosconheceu) {
        res.status(400).json({ id: 0, msg: "Erro... Informe nome, email, fone, estado, cidade, sexo, tiposanguineio e onde nos conheceu." })
        return
    }

    try {
        const doadores = await Doador.create({
            nome, email, fone, estado, cidade, sexo, tiposanguineo, ondenosconheceu
        });
        res.status(201).json(doadores)
    } catch (error) {
        res.status(400).send(error)
    }
}
export const doadorDestroy = async (req, res) => {
    const { id } = req.params
   
    try {
      await Doador.destroy({ where: { id } });

      res.status(200).json({ msg: "Ok! Removido com Sucesso" })
    } catch (error) {
      res.status(400).send(error)
    }
  }
  

export const dadosEstatisticos = async (req, res) => {
    try {
        const numeroDoadores = await Doador.count()
        const somaDoadores = await Doador.sum('nome')
        const TotaldeDoadores = somaDoadores + numeroDoadores

        const numeroDoacoes = await ReceberDoacao.count()
        const somaDoacoes = await ReceberDoacao.sum('nome')
        const TotaldeDoacoes = somaDoacoes + numeroDoacoes

        const numeroContatos = await Contato.count()
        const somaContatos = await Contato.sum('nome')
        const TotaldeContatos = somaContatos + numeroContatos

        res.status(200).json({ TotaldeDoadores, TotaldeDoacoes, TotaldeContatos })
    } catch (error) {
        res.status(400).send(error)
    }
}

export const dadosEstatisticosCount = async (req, res) => {
    try {
        // Contar o número de doadores para cada tipo sanguíneo
        const contagemPorTipoSanguineo = await Doador.findAll({
            attributes: ['tiposanguineo', [sequelize.fn('SUM', 1), 'soma']],
            group: ['tiposanguineo']
        });    

        res.status(200).json(contagemPorTipoSanguineo);
    } catch (error) {
        console.log(error)
        res.status(400).send(error);
    }
}



